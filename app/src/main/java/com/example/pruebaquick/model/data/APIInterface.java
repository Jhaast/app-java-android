package com.example.pruebaquick.model.data;

import com.example.pruebaquick.model.entity.ResultApi;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {

    @GET("/public-api/posts?_format=json&access-token=8r98KCgcMygumizJR1QLxHarTY-7k2p0JrI3")
    Call<ResultApi> getChararacters();
}
