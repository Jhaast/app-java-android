package com.example.pruebaquick.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pruebaquick.R;
import com.example.pruebaquick.model.entity.Result;

import java.util.List;

public class ServiceAdapter
        extends RecyclerView.Adapter<ServiceAdapter.ViewHolderService>
        implements View.OnClickListener{

    List<Result> dataList;
    private View.OnClickListener listener;
    private Context context;

    public ServiceAdapter(List<Result> getData, Context _context) {
        this.dataList = getData;
        this.context = _context;
    }

    @Override
    public ViewHolderService onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,null,false);
        view.setOnClickListener(this);
        return new ViewHolderService(view);
    }


    @Override
    public void onBindViewHolder(ViewHolderService holder, int position) {
        Result r = dataList.get(position);
        holder.userid.setText("USUARIO: "+(r.getUserId() == null ? "Sin usuario": r.getUserId()) );
        holder.title.setText( "TITULO: "+ r.getTitle() );
        holder.body.setText("NOTICIA: "+ r.getBody() );

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View view) {
        if (listener!=null){
            listener.onClick(view);
        }
    }

    public class ViewHolderService extends RecyclerView.ViewHolder {

        TextView userid;
        TextView title;
        TextView body;

        public ViewHolderService(View itemView) {
            super(itemView);
            userid= (TextView) itemView.findViewById(R.id.user_id);
            title= (TextView) itemView.findViewById(R.id.title);
            body= (TextView) itemView.findViewById(R.id.body);
        }
    }
}